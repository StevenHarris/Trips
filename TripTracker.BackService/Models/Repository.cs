﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TripTracker.BackService.Models
{
    public class Repository
    {
        private List<Trip> _myTrips = new List<Trip>
        {
            new Trip
            {
                Id = 1,
                Name = "MVP Summit",
                StartDate = new DateTime(2018, 3, 5),
                EndDate = new DateTime(2018, 3, 8)
            },
            new Trip
            {
                Id = 2,
                Name = "DevIntersection",
                StartDate = new DateTime(2018, 3, 25),
                EndDate = new DateTime(2018, 3, 27)
            },
            new Trip
            {
                Id = 3,
                Name = "Build",
                StartDate = new DateTime(2018, 5, 7),
                EndDate = new DateTime(2018, 5, 9)
            },
        };

        public List<Trip> Get()
        {
            return _myTrips;
        }

        public Trip GetById(int id)
        {
            return _myTrips.FirstOrDefault(x=> x.Id == id);
        }

        public void Add(Trip newTrip)
        {
            _myTrips.Add(newTrip);
        }

        public void Update(Trip tripToUpdate)
        {
            _myTrips.Remove(_myTrips.Find((x=> x.Id == tripToUpdate.Id)));
            _myTrips.Add(tripToUpdate);
        }

        public void Remove(int id)
        {
            _myTrips.Remove(_myTrips.Find((x => x.Id == id)));
        }
    }

}
