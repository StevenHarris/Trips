﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TripTracker.BackService.Models;

namespace TripTracker.BackService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TripsController : ControllerBase
    {
        private readonly Repository _repo;

        public TripsController(Repository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public IEnumerable<Trip> Get()
        {
            return _repo.Get();
        }

        [HttpGet("{id}")]
        public Trip Get(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPost]
        public void Post([FromBody] Trip newTrip)
        {
            _repo.Add(newTrip);
        }

        [HttpPut]
        public void Put([FromBody] Trip newTrip)
        {
            _repo.Update(newTrip);
        }

        [HttpDelete]
        public void Delete(int id)
        {
            _repo.Remove(id);
        }



    }
}